$(function() {

  const MAX_MOB_WIDTH = 767;
  const HEADER_HEIGHT = 426;
  let container = document.getElementsByClassName('container')[0];
  let sideBar = document.getElementsByClassName('sidebar')[0];
  let content = container.getElementsByClassName('content')[0];
  let flexContainer = document.getElementsByClassName('container-flex')[0];

  function toggleClass(element, className) {
    return element.classList.toggle(className);
  }

  function changeSidePosition() {
    let scrollY = window.scrollY;

    if (scrollY > HEADER_HEIGHT && screen.width > MAX_MOB_WIDTH) {
        $('.sidebar').addClass('sidebar_fixed');
        $('.sidebar').removeClass('sidebar_absolute');
    } else {
      $('.sidebar').addClass('sidebar_absolute');
      $('.sidebar').removeClass('sidebar_fixed');
    }
  }

  function toggleTable(e) {
    let row = document.getElementsByTagName('tr');
    let currentInd = [].map.call(row, function(item) { return item }).indexOf(e.target.parentNode);

    for (let i = currentInd + 1; i < row.length; i++) {
      if (!row[i].classList.contains('start')) {
        row[i].classList.toggle('show_row');

      } else {
        break;
      }
    }
  }

  changeSidePosition();

  window.addEventListener('scroll', changeSidePosition);

  $('.sidebar').click(() =>  {
    toggleClass (sideBar, 'sidebar_open')
    toggleClass (sideBar, 'rotate_close');
  });
  $('.header__search').click(() => toggleClass (container, 'search__active'));
  $('.header__close').click(() => toggleClass (container, 'search__active'));
  $('.header__mob-menu').click(() => toggleClass (content, 'mobile-menu__active'));
  $('.menu__link_close-button').click(() => toggleClass (content, 'mobile-menu__active'));

  $('.start').click(toggleTable);
  $('.popup__close-button').click(closePopup);

  function openPopup(className) {
    container.classList.add(`popup__${className}_active`, 'popup_active');
    fillSelects();
  }

  function closePopup() {
    container.classList.remove('popup__visit_active', 'popup_active', 'popup__reference_active', 'popup__ring_active');
  }

  function fillSelects() {
    let selectYear;
    let selectDay;

    if (container.classList.contains('popup__visit_active')) {
      selectYear = $('.select_year_visit')[0];
      selectDay  = $('.select_day_visit')[0];
    } else {
      selectYear = $('.select_year_reference')[0];
      selectDay  = $('.select_day_reference')[0];
    }

    let j = 0;
    for (let i = 2018; i > 1930; i--) {
      selectYear.options[j] = new Option(i, i);
      j++;
    }
    for (let i = 1; i <= 31; i++) {
      selectDay.options[i - 1] = new Option(i, i);
    }
  }

  $('.header__visit').click(() => openPopup('visit'));
  $('.header__phone').click(() => openPopup('ring'));
  $('.header__reference').click(() => openPopup('reference'));
});
