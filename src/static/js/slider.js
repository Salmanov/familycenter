$(document).ready(function() {

  function newsCarousel() {
    if (screen.width > 767) {
      $('.owl-carousel-news').owlCarousel({
        loop: false,
        margin: 10,
        nav: true,
        dots: false,
        navText: [
          "<span class='slide-nav__item slide-nav__item_prev'><img src='../static/img/arrow_prev.png' /></span>",
          "<span class='slide-nav__item slide-nav__item_next'><img src='../static/img/arrow_next.png' /></span>"
        ],
        responsive: {
          0: {
            items: 3
          },
          600: {
            items: 3
          },
          1000: {
            items: 3
          }
        }
      });
    }
  }

  $('.owl-carousel').owlCarousel({
    loop: true,
    smartSpeed: 4000,
    autoplay: false,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    nav: true,
    dots: true,
    navText: [
      "<span class='slide-nav__item slide-nav__item_prev'><img src='../static/img/arrow_prev.png' /></span>",
      "<span class='slide-nav__item slide-nav__item_next'><img src='../static/img/arrow_next.png' /></span>"
    ],

    items: 1,
  });

  $('.owl-carousel2').owlCarousel({
    loop: false,
    margin: 10,
    nav: true,
    dots: false,
    navText: [
      "<span class='slide-nav__item slide-nav__item_prev'><img src='../static/img/arrow_prev.png' /></span>",
      "<span class='slide-nav__item slide-nav__item_next'><img src='../static/img/arrow_next.png' /></span>"
    ],
    responsive: {
      0: {
        items: 2
      },
      600: {
        items: 2
      },
      1000: {
        items: 1
      }
    }
  });

  newsCarousel();
  window.addEventListener('resize',newsCarousel);
});
