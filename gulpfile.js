'use strict';

var gulp = require('gulp'),
  gp = require('gulp-load-plugins')(),
  browserSync = require('browser-sync').create(),
  uglify = require('gulp-uglify-es').default,
  babel = require('gulp-babel');

gulp.task('serve', function() {
  browserSync.init({
    server: {
      baseDir: "./build"
    },
    ui: {
      port: 8000,
      wienre: {
        port: 9090
      }
    }
  });
  browserSync.watch('build', browserSync.reload)
});

gulp.task('nunjucks', function() {
  return gulp.src('src/**.html')
    .pipe(gp.nunjucksRender({
      path: ['src/templates']
    }))
    .pipe(gulp.dest('build/'))
});


gulp.task('image', function() { // Add the newer pipe to pass through newer images only
  return gulp.src('src/static/img/**')
    .pipe(gp.newer('build/static/img'))
    .pipe(gp.image())
    .pipe(gulp.dest('build/static/img'));
});

gulp.task('scripts', function() {
  return gulp.src(['src/static/js/main.js',
                   'src/static/js/slider.js'])
    .pipe(babel({
            presets: ['@babel/env']
        }))
    .pipe(gp.concat('all.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('build/static/js/'));
});

gulp.task('sass', function() {
  return gulp.src('src/static/sass/**.scss')
  .pipe(gp.plumber())
  .pipe(gp.sourcemaps.init()) //remove after debugging
  .pipe(gp.sass({outputStyle: 'compressed'}))
  .pipe(gp.rename({suffix: '.min', prefix : ''}))
  .pipe(gp.autoprefixer(['last 15 versions']))
  .pipe(gp.sourcemaps.write()) //remove after debugging
  .pipe(gulp.dest('build/static/css'))
});

gulp.task('watch', function() {
  gulp.watch('src/static/sass/**/*.scss', gulp.series('sass'));
  gulp.watch('src/static/js/**/*.js', gulp.series('scripts'));
  gulp.watch('src/static/img/*', gulp.series('image'));
  gulp.watch('src/**/*.html', gulp.series('nunjucks'));
})

gulp.task('default', gulp.series(
  gulp.parallel('sass', 'scripts', 'image', 'nunjucks'),
  gulp.parallel('watch', 'serve')
));
